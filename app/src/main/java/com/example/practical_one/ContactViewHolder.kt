package com.example.practical_one

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.text.ParsePosition

class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val imageContact: ImageView = itemView.findViewById(R.id.imageContact)
    private val txtName: TextView = itemView.findViewById(R.id.txtName)
    private val txtPreview: TextView = itemView.findViewById(R.id.txtPreview)


    fun bind(contact: ContactModel) {
        imageContact.setImageResource(contact.image)
        txtName.text = contact.name
        txtPreview.text = contact.msgPreview
    }

}