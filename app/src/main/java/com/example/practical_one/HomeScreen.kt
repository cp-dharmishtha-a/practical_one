package com.example.practical_one

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class HomeScreen : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: ContactListAdapter
    private lateinit var contactList: List<ContactModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_screen)
        recyclerView = findViewById(R.id.recycle_contact)

        val contactList = listOf(
            ContactModel(R.drawable.girlicone, "dharmishtha", "Good Morning"),
            ContactModel(R.drawable.manicon, "jiya", "hiii"),
            ContactModel(R.drawable.redicongirl, "Riya", "How Are You?"),
            ContactModel(R.drawable.manicon, "Diya", "hiii"),
            ContactModel(R.drawable.redicongirl, "Sneha", "How Are You?"),
            ContactModel(R.drawable.manicon, "Diya", "hiii"),
            ContactModel(R.drawable.redicongirl, "Sneha", "How Are You?"),
            ContactModel(R.drawable.redicongirl, "Riya", "How Are You?"),
            ContactModel(R.drawable.manicon, "jiya", "hiii"),
            ContactModel(R.drawable.redicongirl, "Sneha", "How Are You?"),
            ContactModel(R.drawable.manicon, "Diya", "hiii"),
            ContactModel(R.drawable.redicongirl, "Sneha", "How Are You?"),
        )

        adapter = ContactListAdapter(contactList)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

    }
}

