package com.example.practical_one

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    var blogin: Button? = null
    var etusername: EditText? = null
    var etPassword: EditText? = null
    var isAllFieldsChecked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // register  EditText fields with their IDs.
        etusername = findViewById(R.id.username)
        etPassword = findViewById(R.id.password)

        val button = findViewById<Button>(R.id.loginButton)

        // handle the login button
        button.setOnClickListener{

             isAllFieldsChecked = CheckAllFields()
            if (isAllFieldsChecked) {
                   val intent = Intent(this, OnBoarding::class.java)
                  startActivity(intent)
                    finish()
                }
        }

    }
    private fun CheckAllFields(): Boolean {


        if (etusername!!.length() == 0) {
            etusername!!.error = "This field is required"
            return false

        }

        if (etusername!!.text.toString() != "admin" && etPassword!!.text.toString() != "12345678"){
                Toast.makeText(applicationContext,"Please Enter valid Credential",Toast.LENGTH_LONG).show();
                return false;

        }
        if (etPassword!!.length() == 0){
            etPassword!!.error = "password is required"
            return false

        }
        else if (etPassword!!.length() < 8) {
            etPassword!!.error = "Password must be minimum 8 characters"
            return false
        }
        return true
    }
}


