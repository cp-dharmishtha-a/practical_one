package com.example.practical_one

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Switch
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.SwitchCompat
import androidx.viewpager.widget.ViewPager
import com.example.practical_one.R.id.daynight


class OnBoarding: AppCompatActivity(), View.OnClickListener {

    lateinit var skbutton: Button
    lateinit var nextbtn:Button
    lateinit var backbtn:Button
    lateinit var dayNight:SwitchCompat
    lateinit var btnvisi:Button

    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: OnboardAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//       AppCompatDelegate.setDefaultNightMode(
//           AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
//       )
        setContentView(R.layout.activity_on_boarding)
        dayNight = findViewById<SwitchCompat>(R.id.daynight)

//        dayNight.isChecked = false

       dayNight.setOnCheckedChangeListener{ _, isChecked ->

           if (isChecked){
               AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
           }else{
               AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
           }

       }

        // skipbutton
        skbutton = findViewById<Button>(R.id.skip_btn)
        skbutton.setOnClickListener(this)
        //nextbutton
        nextbtn = findViewById<Button>(R.id.next_btn)
        backbtn = findViewById<Button>(R.id.back_btn)

        btnvisi = findViewById<Button>(R.id.get_started_btn)

        viewPager = findViewById(R.id.viewPagerslider)



        skbutton.setOnClickListener{
            val intent = Intent(this, HomeScreen::class.java)
            startActivity(intent)
            finish()
        }
        val onboardingPage = listOf(
            OnboardingPage(R.drawable.callimage, "Make A Call","Lorem Ipsum is simply dummy text of the printing and typesetting industry."),
            OnboardingPage(R.drawable.msgimage, "make Video Call","Lorem Ipsum is simply dummy text of the printing and typesetting industry."),
            OnboardingPage(R.drawable.file, "Share File","Lorem Ipsum is simply dummy text of the printing and typesetting industry."),

        )
        viewPager.adapter = OnboardAdapter(this,onboardingPage)

        nextbtn.setOnClickListener(){
            val currentItem = viewPager.currentItem
            Log.d("variable", "cur variable: $currentItem")

            Log.d("button clicked", "button click")
            if (currentItem < onboardingPage.size){
                viewPager.currentItem = currentItem + 1
                Log.d("inside if", "inside if click")
                Log.d("variable if", "value of cur variable if: ${viewPager.currentItem}")
            }
            if (viewPager.currentItem == viewPager.adapter?.count?.minus(1)){
                        btnvisi.visibility = View.VISIBLE
             }
        }

        btnvisi.setOnClickListener{
            val intent= Intent(this, HomeScreen::class.java)
            startActivity(intent)
            finish()
        }

        backbtn.setOnClickListener{
            if (viewPager.currentItem > 0){
                viewPager.currentItem = viewPager.currentItem - 1
            }
        }
    }

    override fun onClick(p0: View?) {
         TODO("Not yet implemented")
     }

 }






