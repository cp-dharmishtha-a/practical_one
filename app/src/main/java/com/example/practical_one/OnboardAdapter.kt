package com.example.practical_one

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager


class OnboardAdapter(private val context:Context,private val onboardingPage: List<OnboardingPage>): PagerAdapter(){
     var currentPageIndex = 0
     override fun instantiateItem(container : ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.onboard_slides,container,false)
        val page = onboardingPage[position]
        val imageView = view.findViewById<ImageView>(R.id.slider_image)
        val title = view.findViewById<TextView>(R.id.slider_heading)
        val desc = view.findViewById<TextView>(R.id.slider_desc)

        imageView.setImageResource(page.image)
        title.text = page.title
        desc.text = page.desc
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View?)
    }

    override fun getCount(): Int {
        return onboardingPage.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

}












