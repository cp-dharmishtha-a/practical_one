package com.example.practical_one

data class OnboardingPage(val image: Int, val title: String, val desc: String)